

#include "ball_refiner_imgproc.h"
#define DEBUG false


void load_data(cv::Mat& img, RAPSODO_IMAGE_TYPE** short_img)
{
	int img_w = img.cols;
	int img_h = img.rows;

	//create U8 image
	RAPSODO_INT_IMAGE_TYPE* u8_img;
	u8_img = (RAPSODO_INT_IMAGE_TYPE*)calloc(sizeof(RAPSODO_INT_IMAGE_TYPE), 1); 
	imgCreateWIP_U8(u8_img, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
	u8imgResizeBuffer(u8_img, img_w, img_h);

	//create Short image
	// RAPSODO_IMAGE_TYPE* short_img;
	*short_img = (RAPSODO_IMAGE_TYPE*)calloc(sizeof(RAPSODO_IMAGE_TYPE), 1); 
	imgCreateWIP(*short_img, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
	imgResizeBuffer(*short_img, img_w, img_h);

	//copy cv::Mat to U8 image
	U8CvMat2U8Array(img, (uint8_t*) u8_img->image, img_w, img_h);

	//convert U8 image to Short image
	imgConvertu82Short(u8_img, *short_img);

	//destroy U8 image
	imgDestroyWIPINT(u8_img);
	if(u8_img) free(u8_img);
}


int imageGenerateBallFindingTemplateSampled(unsigned short radius, unsigned short thickness,
									  unsigned short *xPosition, unsigned short *yPosition, 
									  short *gradientValue,unsigned short *templateLength)
{
	/*
	To be called during the initialization. To generate the gradient map for a radius,
	based on thickness. 
	unsigned short radius = radius of the circle
	unsigned short thickness = thickness of the ring, to ignore the part inside a circle
	unsigned short *xPosition = non-zero x position of pixels on the circle 
	unsigned short *yPosition = non-zero y position of pixels on the circle
	short *gradientValue = gradient value of the pixel in the same order xPosition and yPosition
	unsigned short *templateLength = length of the array of the template
	*/
	int status = 0,sample;
	unsigned short row, col,tempLength;
	float dist,offsetX,offsetY,fTan;
	float midRow = (2.0f*((float)radius)+1.0f)/2.0f;
	float midCol = (2.0f*((float)radius)+1.0f)/2.0f;
	tempLength = 0;
	sample = (int)((2*MATH_PI*radius/100)+0.5);
	if(sample==0)
	{
		sample = 1;
	}
	for(row=0;row<2*radius;row+=sample)
	{
		for(col=0;col<2*radius;col++)
		{
			offsetX = (float)col-midCol+1.0f;
			offsetY = (float)row-midRow+1.0f;
			dist = sqrtf(offsetX*offsetX+offsetY*offsetY);
			if(dist<radius&&dist>radius-thickness)
			{
				//to calculate the gradient
				fTan = 57.2958f*atan2f(offsetX,offsetY);
				if(fTan<0.0f)
				{
					fTan = fTan-0.5f;
				}
				else
				{
					fTan = fTan+0.5f;
				}
				*(gradientValue+tempLength) = (short)(fTan);
				*(xPosition+tempLength) = col;
				*(yPosition+tempLength) = row;
				tempLength++;
				if(tempLength>TEMPLATE_LENGTH_LIMIT)
				{
					return 1;
				}
			}
		}
	}
	*templateLength = tempLength;
	return status;
}

RAPSODO_ERROR_TYPE ipeInitTemplate(RAPSODO_ENGINE_TYPE *e)
{
	RAPSODO_ERROR_TYPE err=RIPE_SUCCESS;
	unsigned short i;
	int status = 0;
	for(i=TEMPLATE_START;i<TEMPLATE_END_SOFTBALL;i++)
	{
		e->gradientTemplateArray[i-TEMPLATE_START].radius = i;

		e->gradientTemplateArray[i-TEMPLATE_START].xPosition = (unsigned short*)
			malloc(sizeof(unsigned short)*TEMPLATE_LENGTH_LIMIT);

		e->gradientTemplateArray[i-TEMPLATE_START].yPosition = (unsigned short*)
			malloc(sizeof(unsigned short)*TEMPLATE_LENGTH_LIMIT);

		e->gradientTemplateArray[i-TEMPLATE_START].gradientValue = (short*)
			malloc(sizeof(short)*TEMPLATE_LENGTH_LIMIT);

		
		status = imageGenerateBallFindingTemplateSampled(i, 1,
									  e->gradientTemplateArray[i-TEMPLATE_START].xPosition, 
									  e->gradientTemplateArray[i-TEMPLATE_START].yPosition, 
									  e->gradientTemplateArray[i-TEMPLATE_START].gradientValue
									  ,&(e->gradientTemplateArray[i-TEMPLATE_START].templateLength));
									 
									  
		if(status)
		{
			return RIPE_ERROR;
		}
	}
	return err;
}


void initialize_RAPSODO_ENGINE_TYPE(RAPSODO_ENGINE_TYPE** golf_eng)
{
	*golf_eng = (RAPSODO_ENGINE_TYPE*)calloc(sizeof(RAPSODO_ENGINE_TYPE),1); /* Allocate space for the engine */

	// initialize engine
	for(int i=0; i<BASEBALL_MAX_IMAGES; i++)
	{
		imgCreateWIP(&(*golf_eng)->ballForProcessing[i].currentImage, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
		imgCreateWIP(&(*golf_eng)->ballForProcessing[i].absDiffImage, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
		imgCreateWIP(&(*golf_eng)->ballForProcessing[i].croppedImage, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
		imgCreateWIPFloat(&(*golf_eng)->ballForProcessing[i].spinImage, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
		imgCreateWIP_U8(&(*golf_eng)->ballForProcessing[i].claheImage, MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);

		for(int j=0; j<BASEBALL_WIP_NUMBER; j++)
		{
			imgCreateWIP(&(*golf_eng)->ballForProcessing[i].WIP[j], MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
		}
	}
	for(int i=0; i<BASEBALL_WIP_NUMBER * BASEBALL_MAX_IMAGES; i++)
	{
		imgCreateWIP(&(*golf_eng)->WIP[i], MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
		imgCreateWIPFloat(&(*golf_eng)->WIPFLOAT[i], MAX_IMAGE_HEIGHT, MAX_IMAGE_HEIGHT);
	}
	
	ipeInitTemplate((*golf_eng));
}


RAPSODO_ERROR_TYPE ipeDestroyTemplate(RAPSODO_ENGINE_TYPE **e)
{
	RAPSODO_ERROR_TYPE err=RIPE_SUCCESS;
	unsigned short i;
	for(i=TEMPLATE_START;i<TEMPLATE_END_SOFTBALL;i++)
	{
		free((*e)->gradientTemplateArray[i-TEMPLATE_START].xPosition);

		free((*e)->gradientTemplateArray[i-TEMPLATE_START].yPosition);

		free((*e)->gradientTemplateArray[i-TEMPLATE_START].gradientValue);
	}
	return err;
}

void destroy_RAPSODO_ENGINE_TYPE(RAPSODO_ENGINE_TYPE** golf_eng)
{
	// initialize engine
	for(int i=0; i<BASEBALL_MAX_IMAGES; i++)
	{
		imgDestroyWIP(&(*golf_eng)->ballForProcessing[i].currentImage);
		imgDestroyWIP(&(*golf_eng)->ballForProcessing[i].absDiffImage);
		imgDestroyWIP(&(*golf_eng)->ballForProcessing[i].croppedImage);
		imgDestroyWIPFLOAT(&(*golf_eng)->ballForProcessing[i].spinImage);
		imgDestroyWIPINT(&(*golf_eng)->ballForProcessing[i].claheImage);

		for(int j=0; j<BASEBALL_WIP_NUMBER; j++)
		{
			imgDestroyWIP(&(*golf_eng)->ballForProcessing[i].WIP[j]);
		}
	}
	for(int i=0; i<BASEBALL_WIP_NUMBER * BASEBALL_MAX_IMAGES; i++)
	{
		imgDestroyWIP(&(*golf_eng)->WIP[i]);
		imgDestroyWIPFLOAT(&(*golf_eng)->WIPFLOAT[i]);
	}
	ipeDestroyTemplate(golf_eng);
}



RAPSODO_ERROR_TYPE ball_fine_tuning(RAPSODO_ENGINE_TYPE *eng, int index, int threadIndex)
{

	RAPSODO_ERROR_TYPE err = RIPE_SUCCESS;

	RAPSODO_BALL_PARAM *ballParam = &(eng->ballForProcessing[index].ball);
	RAPSODO_PROCESS_BALL_TYPE *currentBall = &(eng->ballForProcessing[index]); 

	RAPSODO_BALL_PARAM secondBallParam;
	secondBallParam.ballConfidence = 0.0f;
	secondBallParam.centerX = 0.0f;
	secondBallParam.centerY = 0.0f;
	secondBallParam.ballRadius = 0.0f;


	RAPSODO_IMAGE_TYPE *pWIP = &eng->WIP[BASEBALL_WIP_NUMBER*threadIndex];
	RAPSODO_IMAGE_TYPE_FLOAT *pWIPFLOAT = &eng->WIPFLOAT[BASEBALL_WIP_NUMBER*threadIndex];

	RAPSODO_IMAGE_TYPE_FLOAT wipFL[BASEBALL_WIP_NUMBER];
	RAPSODO_IMAGE_TYPE wip[BASEBALL_WIP_NUMBER];

	

	float cRad,sum;
	unsigned int uintRadius;
	int ROIrowStart,ROIrowEnd,ROIcolStart,ROIcolEnd;
	int srcRowStart,srcRowEnd,srcColStart,srcColEnd;
	int i;
	RAPSODO_IMAGE_TYPE current, *croppedImage;
	RAPSODO_IMAGE_TYPE_FLOAT *spinImage;
	
	
	RAPSODO_INT_IMAGE_TYPE *wipU8;
	float thresh;
	unsigned short *gradArrayX,*gradArrayY;
	int thsMapLength;
	int j;
	float scale;
	unsigned int rad = 60;


	wipU8 = &(currentBall->claheImage);
	for(i=0;i<BASEBALL_WIP_NUMBER;i++)
	{
		wip[i] = pWIP[i];
	}
	for(i=0;i<BASEBALL_WIP_NUMBER;i++)
	{
		wipFL[i] = pWIPFLOAT[i];
	}

	current = currentBall->currentImage;
	croppedImage = &(currentBall->croppedImage);
	spinImage = &(currentBall->spinImage);

	
	// ********************************** crop the images and resize ***********************************************//
	// 1) resize the ball to ref radius for ball finding finetuning
	cRad = (ballParam->ballRadius + 12.0f);
	uintRadius = (int)(cRad); // original + error margin 
	srcRowStart = (int)ballParam->centerY - uintRadius;
	ROIrowStart = 0; // This is for destination image
	srcRowEnd	= (int)ballParam->centerY + uintRadius;
	ROIrowEnd   = 2 * uintRadius; // This is for destination image
	
	

	srcColStart = (int)ballParam->centerX - uintRadius;
	ROIcolStart = 0; // This is for destination image
	srcColEnd	= (int)ballParam->centerX + uintRadius;
	ROIcolEnd   = 2 * uintRadius; // This is for destination image

	// 1a) check out of bound
	/* if out of bound */
	if(srcRowStart < 0)
	{
		ROIrowStart = ( 2 * uintRadius) - srcRowEnd;
		srcRowStart = 0;
		eng->ballFoundCount--;
		currentBall->ballFound = RIPE_BALL_CROPPED;
		return RIPE_ERROR_NO_BALL;
	}
	
	if(srcColStart < 0)
	{

		ROIcolStart = ( 2 * uintRadius) - srcColEnd;
		srcColStart = 0;
		eng->ballFoundCount--;
		currentBall->ballFound = RIPE_BALL_CROPPED;
		return RIPE_ERROR_NO_BALL;
	}
	
	if(srcRowEnd > current.height)
	{
		ROIrowStart = 0;
		srcRowEnd = current.height;
		eng->ballFoundCount--;
		currentBall->ballFound = RIPE_BALL_CROPPED;
		return RIPE_ERROR_NO_BALL;
	}
	
	if(srcColEnd > current.width)
	{
		ROIcolStart = 0;
		srcColEnd = current.width;
		eng->ballFoundCount--;
		currentBall->ballFound = RIPE_BALL_CROPPED;
		return RIPE_ERROR_NO_BALL;
	}
	
	/* all wip buffer available */
	imgResizeBuffer(&wip[1], 2*uintRadius, 2*uintRadius);
	imgResizeBuffer(croppedImage, 2*uintRadius, 2*uintRadius);


	
	// 1b) copy ball image to resized buffer
	imgCopyROIShort(&current, &wip[1], srcRowStart, srcColStart, srcRowEnd, srcColEnd, ROIrowStart, ROIcolStart);
	imgCopyROIShort(&current, croppedImage, srcRowStart, srcColStart, srcRowEnd, srcColEnd, ROIrowStart, ROIcolStart);

	if(DEBUG)
	{
		cv::Mat temp_img0 = cv::Mat::zeros(cv::Size(croppedImage->width, croppedImage->height), CV_32F);
		shortArray2FloatCvMat(temp_img0, (short*)(croppedImage->image), croppedImage->width, croppedImage->height);
		double minVal0; 
		double maxVal0; 
		cv::Point minLoc0; 
		cv::Point maxLoc0;
		cv::minMaxLoc( temp_img0, &minVal0, &maxVal0, &minLoc0, &maxLoc0);
		temp_img0 = (temp_img0 - minVal0)/maxVal0;
		cv::resize(temp_img0, temp_img0, cv::Size(2*temp_img0.cols, 2*temp_img0.rows));
		cv::imshow("original ball image", temp_img0);
		char k;
		k = cv::waitKey(0);
		if(k == 27) cv::destroyAllWindows();
	}




	// 1c) bicubic resize
	imgResizeBuffer(&wip[3], 2*rad, 2*rad);  // resize to 120x120 image --- rad = 60
	imgBiCubicResizeQ15(wip[1].image, wip[3].image, 2*uintRadius, 2*uintRadius, 2*rad, 2*rad);

	if(DEBUG)
	{
		cv::Mat temp_img1 = cv::Mat::zeros(cv::Size(wip[3].width, wip[3].height), CV_32F);
		shortArray2FloatCvMat(temp_img1, (short*)(wip[3].image), wip[3].width, wip[3].height);
		double minVal1; 
		double maxVal1; 
		cv::Point minLoc1; 
		cv::Point maxLoc1;
		cv::minMaxLoc( temp_img1, &minVal1, &maxVal1, &minLoc1, &maxLoc1);
		temp_img1 = (temp_img1 - minVal1)/maxVal1;
		cv::resize(temp_img1, temp_img1, cv::Size(2*temp_img1.cols, 2*temp_img1.rows));
		cv::imshow("resized ball image", temp_img1);
		char k;
		k = cv::waitKey(0);
		if(k == 27) cv::destroyAllWindows();
	}

	imgResizeBufferFloat(spinImage, 2*rad, 2*rad);  // resize to 120x120 image --- rad = 60
	//copy to wip2
	imgResizeBuffer(&wip[2], 2*rad, 2*rad);  // resize to 120x120 image --- rad = 60
	imgSetQ15(&(wip[2]), 0x7fff); // set 15 bit
	imgCopyROIShort(&wip[3], &wip[2], 0, 0, 2*rad, 2*rad, 0, 0);

	// ********************************** crop the images and resize ***********************************************//


	// wip[1]: orig. size ball, wip[3]: resized ball, wip[2]:resized ball


	// ********************************** histogram equalization with CLAHE ***********************************************//

	// 2)  clahe to enhance the edges 
	imgFineTuningOutdoor(eng, wipU8, &wip[2], &wip[0], &wip[1], &wip[3], &wip[4], &wip[5], spinImage);

	imgConvertFloat2u8(spinImage, wipU8);

	imgResizeBuffer(&(wip[0]), wipU8->width, wipU8->height); //wip4
	imgResizeBuffer(&(wip[1]), wipU8->width, wipU8->height); //wip6
	imgResizeBuffer(&(wip[2]), wipU8->width, wipU8->height); //wip3
	imgResizeBuffer(&(wip[5]), wipU8->width, wipU8->height); 
	imgResizeBufferFloat((pWIPFLOAT), wipU8->width, wipU8->height); //wip4

	// ********************************** histogram equalization with CLAHE ***********************************************//


	// spinImage: Histogram equalized img. , wipU8: Histogram equalized img. 


	// ********************************** edge magnitude and angle detection with Sobel ***********************************************//



	//2) Perform gradient direction and magnitude calculation on original image.
	combineSobelMagnitudeGradientIPPWrapQ15(wipU8->image, wip[5].image, (float *)pWIPFLOAT->image, wip[2].image, wip[3].image, wipU8->width, wipU8->height);

	if(DEBUG)
	{
		cv::Mat temp_img2 = cv::Mat::zeros(cv::Size(spinImage->width, spinImage->height), CV_32F);
		floatArray2FloatCvMat(temp_img2, (float*)(spinImage->image), spinImage->width, spinImage->height);
		double minVal2; 
		double maxVal2; 
		cv::Point minLoc2; 
		cv::Point maxLoc2;
		cv::minMaxLoc( temp_img2, &minVal2, &maxVal2, &minLoc2, &maxLoc2 );
		temp_img2 = (temp_img2-minVal2)/maxVal2;
		cv::resize(temp_img2, temp_img2, cv::Size(2*temp_img2.cols,2*temp_img2.rows));


		cv::Mat temp_img3 = cv::Mat::zeros(cv::Size(wip[5].width, wip[5].height), CV_32F);
		shortArray2FloatCvMat(temp_img3, (dtypeipe*)(wip[5].image), wip[5].width, wip[5].height);
		double minVal3; 
		double maxVal3; 
		cv::Point minLoc3; 
		cv::Point maxLoc3;
		cv::minMaxLoc( temp_img3, &minVal3, &maxVal3, &minLoc3, &maxLoc3 );
		temp_img3 = (temp_img3-minVal3)/maxVal3;
		cv::resize(temp_img3, temp_img3, cv::Size(2*temp_img3.cols,2*temp_img3.rows));
		

		cv::Mat temp_img4 = cv::Mat::zeros(cv::Size(pWIPFLOAT->width, pWIPFLOAT->height), CV_32F);
		floatArray2FloatCvMat(temp_img4, (float*)(pWIPFLOAT->image), pWIPFLOAT->width, pWIPFLOAT->height);
		double minVal4; 
		double maxVal4; 
		cv::Point minLoc4; 
		cv::Point maxLoc4;
		cv::minMaxLoc( temp_img4, &minVal4, &maxVal4, &minLoc4, &maxLoc4 );
		temp_img4 = (temp_img4-minVal4)/maxVal4;
		cv::resize(temp_img4, temp_img4, cv::Size(2*temp_img4.cols,2*temp_img4.rows));

		cv::imshow("clahe", temp_img2);
		cv::imshow("edge magnitude", temp_img3);
		cv::imshow("angle magnitude", temp_img4);
		char k;
		k = cv::waitKey(0);
		if(k == 27) cv::destroyAllWindows();

	}

	// ********************************** edge magnitude and angle detection with Sobel ***********************************************//

	// wip[5]: edge magnitude, pWIPFLOAT: edge angle

	gradArrayX = (unsigned short*)wip[1].image;
	gradArrayY = (unsigned short*)wip[3].image;

	thsMapLength = 0;
	for(i = -12; i <= 12; i++)
		for (j = -12; j <=12; j++)
		{
			gradArrayX[thsMapLength] = (unsigned short)(rad) + i;
			gradArrayY[thsMapLength++] = (unsigned short)(rad) + j;
		}

	ballParam->ballConfidence = 10000000.0f;

	//init heatmap
	imgResizeBuffer(&(wip[2]),wipU8->width,wipU8->height);
	imgResizeBuffer(&(wip[4]),wipU8->width,wipU8->height);
	imgSetQ15(&(wip[2]),0x7fff);
	//init radius map
	imgResizeBuffer(&(wip[4]),wip[0].width,wip[0].height);

	if(eng->isOutdoor == 0) // indoor
	{
		if(index < eng->level1BallCount)
		{
			thresh = 0x0012;
		}
		else
		{
			thresh = 0x0050;
		}
	}
	else //outdoor
	{
		thresh = 0x0032;
	}

	// for(i=rad - 26; i<=rad - 6;i++)
	// for(i=TEMPLATE_START; i<=TEMPLATE_END_SOFTBALL;i++)
	for(i=TEMPLATE_START; i<=rad;i++)
	{
		imageGradientBallFinding2Q15(pWIPFLOAT->image, wip[5].image, wip[4].image,
			wip[2].image, gradArrayX, gradArrayY,
			thsMapLength,
			eng->gradientTemplateArray[i-TEMPLATE_START].gradientValue,
			eng->gradientTemplateArray[i-TEMPLATE_START].yPosition,
			eng->gradientTemplateArray[i-TEMPLATE_START].xPosition,
			(float)eng->gradientTemplateArray[i-TEMPLATE_START].radius,
			eng->gradientTemplateArray[i-TEMPLATE_START].templateLength,
			wip[0].height, wip[0].width,
			2*eng->gradientTemplateArray[i-TEMPLATE_START].radius,
			2*eng->gradientTemplateArray[i-TEMPLATE_START].radius,
			&secondBallParam.centerY,&secondBallParam.centerX,&secondBallParam.ballConfidence,thresh);
	}

	if(DEBUG)
	{
		cv::Mat temp_img5 = cv::Mat::zeros(cv::Size(wip[2].width, wip[2].height), CV_32F);
		shortArray2FloatCvMat(temp_img5, (short*)(wip[2].image), wip[2].width, wip[2].height);
		double minVal5; 
		double maxVal5; 
		cv::Point minLoc5; 
		cv::Point maxLoc5;
		cv::minMaxLoc( temp_img5, &minVal5, &maxVal5, &minLoc5, &maxLoc5 );
		temp_img5 = (temp_img5-minVal5)/maxVal5;
		cv::resize(temp_img5, temp_img5, cv::Size(2*temp_img5.cols,2*temp_img5.rows));
		cv::imshow("heatmap", temp_img5);
		char k;
		k = cv::waitKey(0);
		if(k == 27) cv::destroyAllWindows();
	}


	// 5) Find the local 2D min with window to find a number of min peak across the image.
	//find local min
	imageFind2DLocalMinOptimizedQ15 (wip[2].image, wip[2].width, wip[2].height, 10,
		20,gradArrayY,gradArrayX,thsMapLength,
		&(eng->ballForProcessing[index].pixelPositionX[0]),
		&(eng->ballForProcessing[index].pixelPositionY[0]));

	i = 0;

	eng->ballForProcessing[index].imagingRadius[i] =
		(float)*(wip[4].image + (unsigned)eng->ballForProcessing[index].pixelPositionX[i]
	+ (unsigned)eng->ballForProcessing[index].pixelPositionY[i]*wip[4].width);

	scale = (rad/(cRad));
	if(eng->isOutdoor == 0)
		eng->ballForProcessing[index].ball.ballRadius = (eng->ballForProcessing[index].imagingRadius[i]+0.75f)/scale;
	else
		eng->ballForProcessing[index].ball.ballRadius = (eng->ballForProcessing[index].imagingRadius[i]+0.75f)/scale;

	eng->ballForProcessing[index].ball.centerX += (eng->ballForProcessing[index].pixelPositionX[i] - rad)/scale + 0.5f;
	eng->ballForProcessing[index].ball.centerY += (eng->ballForProcessing[index].pixelPositionY[i] - rad)/scale + 0.5f;

	return RIPE_SUCCESS;
}



int main()
{
	std::string img_pth = "/home/rapsodo/workspace/ball_fine_tuning/data/1/frame_9_exp_26_gain_3.png";

	//load image
	cv::Mat img = cv::imread(img_pth, 0);

	cv::Mat img0=img.clone();
	cv::Mat img1=img.clone();

	//Create RAPSODO_IMAGE_TYPE from image
	RAPSODO_IMAGE_TYPE* short_img;
	load_data(img, &short_img);

	//load ball param
	//BALL1-caseb
	RAPSODO_BALL_PARAM ball_param;
	ball_param.centerX = 594;
	ball_param.centerY = 210;
	ball_param.ballRadius = 16;
	ball_param.ballConfidence = 1.0;


	int x0=ball_param.centerX-ball_param.ballRadius;
	int y0=ball_param.centerY-ball_param.ballRadius;
	int r0=(int)ball_param.ballRadius;


	RAPSODO_ENGINE_TYPE* golf_eng;
	initialize_RAPSODO_ENGINE_TYPE(&golf_eng);

	imgResizeBuffer(&golf_eng->ballForProcessing[0].currentImage, short_img->width, short_img->height);
	imgCopyROIShort(short_img, &(golf_eng->ballForProcessing[0].currentImage), 0, 0,  short_img->height, short_img->width, 0, 0);
	imgDestroyWIP(short_img);
	if(short_img) free(short_img);

	golf_eng->ballForProcessing[0].ball = ball_param;
	golf_eng->isOutdoor = 1;

	ball_fine_tuning(golf_eng,0, 0);


	int x1=golf_eng->ballForProcessing[0].ball.centerX-golf_eng->ballForProcessing[0].ball.ballRadius;
	int y1=golf_eng->ballForProcessing[0].ball.centerY-golf_eng->ballForProcessing[0].ball.ballRadius;
	int r1=(int)golf_eng->ballForProcessing[0].ball.ballRadius;

	// cv::cvtColor(img, img0, cv::COLOR_GRAY2BGR);
	// cv::cvtColor(img, img1, cv::COLOR_GRAY2BGR);
	
	// cv::Rect rect0 = cv::Rect(x0, y0, 2*r0, 2*r0);
	// cv::Rect rect1 = cv::Rect(x1, y1, 2*r1, 2*r1);
	// cv::rectangle(img0, rect0, cv::Scalar(0, 0, 255), 1);
	// cv::rectangle(img1, rect1, cv::Scalar(0, 255, 0), 1);

	// cv::imshow("full_img0", img0);
	// cv::imshow("full_img1", img1);
	// cv::waitKey(0);



	destroy_RAPSODO_ENGINE_TYPE(&golf_eng);
	if(golf_eng) free(golf_eng);

	return 0;
}