#ifndef __BALL_REFINER_IMGPROC_H__
#define __BALL_REFINER_IMGPROC_H__

#include "clahe.h"
#include "ball_refiner_utils.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>



//**************************************************** OpenCV-Array Converters *****************************************//


void U8CvMat2ShortArray(const cv::Mat& img, dtypeipe* arr, int width, int height)
{
	for(int i=0; i<width; i++)
	{
		for(int j=0; j<height; j++)
		{
			*(arr + i + j*width) = (dtypeipe)img.at<uint8_t>(j,i);
		}
	}
}

void U8CvMat2U8Array(const cv::Mat& img, uint8_t* arr, int width, int height)
{
	for(int i=0; i<width; i++)
	{
		for(int j=0; j<height; j++)
		{
			*(arr + i + j*width) = (uint8_t)img.at<uint8_t>(j,i);
		}
	}
}

void shortArray2U8cvMat(cv::Mat& img, dtypeipe* arr, int width, int height)
{
	for(int i=0; i<width; i++)
	{
		for(int j=0; j<height; j++)
		{
			img.at<uint8_t>(j,i) = (uint8_t)(*(arr + i + j*width));
		}
	}
}

void shortArray2FloatCvMat(cv::Mat& img, dtypeipe* arr, int width, int height)
{
	for(int i=0; i<width; i++)
	{
		for(int j=0; j<height; j++)
		{
			img.at<float>(j,i) = (float)(*(arr + i + j*width));
		}
	}
}

void floatArray2FloatCvMat(cv::Mat& img, float* arr, int width, int height)
{
	for(int i=0; i<width; i++)
	{
		for(int j=0; j<height; j++)
		{
			img.at<float>(j,i) = (float)(*(arr + i + j*width));
		}
	}
}

void U8Array2U8CvMat(cv::Mat& img, uint8_t* arr, int width, int height)
{
	for(int i=0; i<width; i++)
	{
		for(int j=0; j<height; j++)
		{
			img.at<uint8_t>(j,i) = (uint8_t)(*(arr + i + j*width));
		}
	}
}

//**************************************************** OpenCV-Array Converters *****************************************//



//**************************************************** Buffer Resizers *****************************************//

static void imgResizeBuffer(RAPSODO_IMAGE_TYPE *src, unsigned int width, unsigned int height)
{
	src->width = width;
	src->height = height;
	memset(src->image, 0, sizeof(dtypeipe) * src->width * src->height);
}

static void u8imgResizeBuffer(RAPSODO_INT_IMAGE_TYPE *src, unsigned int width, unsigned int height)
{
	src->width = width;
	src->height = height;
	memset(src->image,0,sizeof(unsigned char)*src->width*src->height);
}


static void imgResizeBufferFloat(RAPSODO_IMAGE_TYPE_FLOAT *src, unsigned int width, unsigned int height)
{
	src->width = width;
	src->height = height;
	memset(src->image,0,sizeof(float)*src->width*src->height);
}

//**************************************************** Buffer Resizers *****************************************//



//**************************************************** Data Copyers *****************************************//

static void imgCopyROI(RAPSODO_IMAGE_TYPE_FLOAT* src,RAPSODO_IMAGE_TYPE_FLOAT* dest,unsigned int srcRowStart,unsigned int srcColStart,unsigned int srcRowEnd,unsigned int srcColEnd, unsigned int destRowStart, unsigned int destColStart)
{
	unsigned int row;

	for(row=srcRowStart; row < srcRowEnd ;row++)
	{
		memcpy(dest->image+destColStart+(row-srcRowStart+destRowStart)*dest->width,src->image + srcColStart + row * src->width,sizeof(float)*(srcColEnd - srcColStart));
	}
}
static void imgCopyROIShort(RAPSODO_IMAGE_TYPE* src,RAPSODO_IMAGE_TYPE* dest,unsigned int srcRowStart,unsigned int srcColStart,unsigned int srcRowEnd,unsigned int srcColEnd, unsigned int destRowStart, unsigned int destColStart)
{
	unsigned int row;

	for(row=srcRowStart; row < srcRowEnd ;row++)
	{
		memcpy(dest->image+destColStart+(row-srcRowStart+destRowStart)*dest->width,src->image + srcColStart + row * src->width,sizeof(dtypeipe)*(srcColEnd - srcColStart));
	}
}

//**************************************************** Data Copyers *****************************************//


//**************************************************** Interpolators *****************************************//

static float cubicInterpolate (float p[4], float x) 
{
	return p[1] + 0.5f * x*(p[2] - p[0] + x*(2.0f*p[0] - 5.0f*p[1] + 4.0f*p[2] - p[3] + x*(3.0f*(p[1] - p[2]) + p[3] - p[0])));
}

static float bicubicInterpolate (float p[4][4], float x, float y) {
	float arr[4];
	float temp;
	arr[0] = cubicInterpolate(p[0], y);
	arr[1] = cubicInterpolate(p[1], y);
	arr[2] = cubicInterpolate(p[2], y);
	arr[3] = cubicInterpolate(p[3], y);
	temp = cubicInterpolate(arr, x);
	if(temp>1022)
	{
		temp = 1022;
	}
	if(temp<0)
	{
		temp = 0;
	}
	return temp;
}


void imgBiCubicResizeQ15(dtypeipe *src,dtypeipe *dest, 
	unsigned int srcHeight, unsigned int srcWidth, unsigned int destHeight, unsigned int destWidth )
{
	unsigned int row, col,i;
	int sH, sW, dH, dW;
	float tempRow, tempCol;
	int floorRow, floorCol;
	float resRow, resCol;
	float window[4][4];
	float iScale = (float)srcHeight/(float)destHeight; //for bicubic, no. bigger than one = downsizing

	dH = (int)destHeight;
	dW = (int)destWidth;

	sH = (int)srcHeight;
	sW = (int)srcWidth;
	for(row=0;row<destHeight;row++)
	{
		for(col=0;col<destWidth;col++)
		{
			tempRow = (float)row*iScale;
			tempCol = (float)col*iScale;
			floorRow = (int)tempRow;
			floorCol = (int)tempCol;
			resRow = tempRow - (float)floorRow;
			resCol = tempCol - (float)floorCol;
			if(floorRow>0 && floorCol>0 && floorRow+2<sH && floorCol+2<sW)
			{
				for(i =0;i<4;i++)
				{
					window[0][i] = (*(src+floorCol-1+(floorRow-1)*sW+i))* 0.0156f;  // 256/16384
					window[1][i] = (*(src+floorCol-1+(floorRow)*sW+i))* 0.0156f;
					window[2][i] = (*(src+floorCol-1+(floorRow+1)*sW+i))* 0.0156f;
					window[3][i] = (*(src+floorCol-1+(floorRow+2)*sW+i))* 0.0156f;
				}

				*(dest+col+row*dW) = (dtypeipe)(128.0f*bicubicInterpolate(window,resRow,resCol));
				if(*(dest+col+row*dW)>32767)
				{
					*(dest+col+row*dW) = 32767;
				}
				else if(*(dest+col+row*dW)<0)
				{
					*(dest+col+row*dW) = 32767;
				}
			}
			else
			{
				if(floorRow>=0 && floorCol>=0 && floorRow<sH && floorCol<sW)
				{
					*(dest+col+row*dW) = *(src+floorCol+floorRow*sW);
				}
			}
		}
	}
}


//**************************************************** Interpolators *****************************************//


//**************************************************** Image Type Convertes *****************************************//

static void imgConvertShort2u82(RAPSODO_IMAGE_TYPE *src,RAPSODO_INT_IMAGE_TYPE *dest)
{
	unsigned int i=0;
	dtypeipe *p=src->image;
	unsigned char *q=dest->image;
	float temp;


	for (i=0; i<src->height*src->width; i++)
	{
		temp = (float)(*(p+i));
		temp /= (16384.0f*2.0f);
		temp *= 256.0f;
		if(temp >= 255.0f)
			temp = 255.0f;

		*(q+i)=(unsigned char)(temp);
	}
}

static void imgConvertu82Short(RAPSODO_INT_IMAGE_TYPE *src,RAPSODO_IMAGE_TYPE *dest)
{
	unsigned int i=0;
	unsigned char *p=src->image;
	dtypeipe *q=dest->image;
	for (i=0; i<src->height*dest->width; i++)
	{
		//*(q+i)=(float)(*(p+i))/((float)IMAGE_MAX_PIXEL_VALUE*);
		*(q+i) = (dtypeipe)((*(p+i))/((float)IMAGE_MAX_PIXEL_VALUE+1.0f)*FP_CONST);
	}
}

static void imgConvertu82FloatImage(RAPSODO_INT_IMAGE_TYPE *src,RAPSODO_IMAGE_TYPE_FLOAT *dest)
{
	unsigned int i=0;
	unsigned char *p=src->image;
	float *q=dest->image;
	for (i=0; i<src->height*dest->width; i++)
	{
		*(q+i)=(float)(*(p+i))/((float)(IMAGE_MAX_PIXEL_VALUE+1));
	}
}

static void imgConvertFloat2u8(RAPSODO_IMAGE_TYPE_FLOAT *src,RAPSODO_INT_IMAGE_TYPE *dest)
{
	unsigned int i=0;
	float *p=src->image;
	unsigned char *q=dest->image;


	for (i=0; i<src->height*src->width; i++)
	{
		*(q+i)=(unsigned char)((*(p+i)*IMAGE_MAX_PIXEL_VALUE)+0.0f);
	}
}


static void imgConvertShort2Float(RAPSODO_IMAGE_TYPE *src,RAPSODO_IMAGE_TYPE_FLOAT *dest)
{
	unsigned int i=0;
	dtypeipe *p=src->image;
	float *q=dest->image;
	float temp;


	for (i=0; i<src->height*src->width; i++)
	{
		temp = (float)(*(p+i));
		temp /= (16384.0f*2.0f);
		temp *= 1.0f;
		if(temp >= 1.0f)
			temp = 1.0f;

		*(q+i)=(float)(temp);
	}
}

//**************************************************** Image Type Convertes *****************************************//


//**************************************************** Data Creators *****************************************//


RAPSODO_ERROR_TYPE imgCreateWIP(RAPSODO_IMAGE_TYPE *img, int width, int height)
{
	img->image=(dtypeipe *)malloc(sizeof(dtypeipe)*width*height);

	if (img->image)
	{
		img->height=height;
		img->width=width;
		ZERO_MEMORY(img->image,sizeof(dtypeipe)*width*height);
		return RIPE_SUCCESS;
	} 
	else
	{
		return RIPE_ERROR;
	}
}


RAPSODO_ERROR_TYPE imgCreateWIPFloat(RAPSODO_IMAGE_TYPE_FLOAT *img, int width, int height)
{
	img->image=(float *)malloc(sizeof(float)*width*height);

	if (img->image)
	{
		img->height=height;
		img->width=width;
		ZERO_MEMORY(img->image,sizeof(float)*width*height);
		return RIPE_SUCCESS;
	} 
	else
	{
		return RIPE_ERROR;
	}
}


RAPSODO_ERROR_TYPE imgCreateWIP_U8(RAPSODO_INT_IMAGE_TYPE *img, int width, int height)
{
	img->image=(unsigned char*)malloc(sizeof(unsigned char)*width*height);

	if (img->image)
	{	
		img->height=height;
		img->width=width;
		ZERO_MEMORY(img->image,sizeof(unsigned char)*width*height);
		return RIPE_SUCCESS;
	} 
	else
	{
		return RIPE_ERROR;
	}
}


//**************************************************** Data Creators *****************************************//



//**************************************************** Data Destroyers *****************************************//



RAPSODO_ERROR_TYPE imgDestroyWIP(RAPSODO_IMAGE_TYPE *img)
{
	if (img->image)
	free(img->image);


	return RIPE_SUCCESS;
}

RAPSODO_ERROR_TYPE imgDestroyWIPFLOAT(RAPSODO_IMAGE_TYPE_FLOAT *img)
{
	if (img->image)
	free(img->image);


	return RIPE_SUCCESS;
}

RAPSODO_ERROR_TYPE imgDestroyWIPINT(RAPSODO_INT_IMAGE_TYPE *img) {
  if (img->image) free(img->image);

  return RIPE_SUCCESS;
}

//**************************************************** Data Destroyers *****************************************//



//**************************************************** Histogram Equalizer *****************************************//


static void imgAdaptiveHistogram(RAPSODO_INT_IMAGE_TYPE *inputImage, RAPSODO_INT_IMAGE_TYPE *outputImage, claheParamsType *params)
{
	/* Call the right API */

	int status=0;
	int i,minNumber = 255,maxNumber = 0;

	for(i=0;i<params->widthRes*params->heightRes;i++)
	{
		if(*(outputImage->image+i)>maxNumber)
		{
			maxNumber = *(outputImage->image+i);
		}
		if(*(outputImage->image+i)<maxNumber)
		{
			minNumber = *(outputImage->image+i);
		}
	}
	
	status = CLAHE((kz_pixel_t*)outputImage->image, params->widthRes, params->heightRes, params->minPixValue, params->maxPixValue, params->widthQuantum, params->heightQuantum, params->bins,params->clipLimit);
	//status = CLAHE(outputImage->image,params->widthRes,params->heightRes,minNumber,maxNumber,params->widthQuantum,params->heightQuantum,params->bins,params->clipLimit);
}


static void imgFineTuningOutdoor(RAPSODO_ENGINE_TYPE *eng, RAPSODO_INT_IMAGE_TYPE *wipU8, RAPSODO_IMAGE_TYPE *original, RAPSODO_IMAGE_TYPE *wip0, 
	RAPSODO_IMAGE_TYPE *wip1, RAPSODO_IMAGE_TYPE *wip2, RAPSODO_IMAGE_TYPE *wip3,RAPSODO_IMAGE_TYPE *wip4,
	RAPSODO_IMAGE_TYPE_FLOAT *output)
{
	//1) perform polar transform
	//2) pad right and left
	//3) clahe
	//4) truncate
	//5) polar to cart
	// do cart clahe
	float sum;
	claheParamsType clParam;
	clParam.bins = CLAHE_BIN_COUNT;
	clParam.clipLimit = 255*0.01;
	// clParam.clipLimit = 10; //BY
	clParam.maxPixValue = CLAHE_MAX_PIX_VAL;
	clParam.minPixValue = CLAHE_MIN_PIX_VAL;
	clParam.widthQuantum = 10;
	clParam.heightQuantum = 10;
	// clParam.widthQuantum = 2; //BY
	// clParam.heightQuantum = 2; //BY
	clParam.widthRes = 120;
	clParam.heightRes = 120;

	
	imgResizeBuffer(wip4, original->width, original->height);
	u8imgResizeBuffer(wipU8, wip4->width, wip4->height);
	//filter2D(original, wip1, wip2, &hBox2[0][0], bHeight);
	imgConvertShort2u82(original, wipU8);
	//RIPEGetTime(&timeIt.imgProcessPostProcess3[imageIndex], NULL);

	imgAdaptiveHistogram(wipU8, wipU8, &clParam);
	//RIPEGetTime(&timeIt.imgProcessHistogram[imageIndex], NULL);

	imgResizeBuffer(wip0, wip4->width,wip4->height);
	output->width = wip4->width;
	output->height = wip4->height;
	imgConvertu82Short(wipU8, wip0);

	// cv::Mat temp_img2 = cv::Mat::zeros(cv::Size(wip0->width, wip0->height), CV_32F);
	// array2cvMatFloat(temp_img2, (dtypeipe*)(wip0->image), wip0->width, wip0->height);
	// cv::imshow("temp_img2", temp_img2);
	// cv::waitKey(0);

	imgConvertu82FloatImage(wipU8, output);
}


//**************************************************** Histogram Equalizer *****************************************//



//**************************************************** Edge Detecor - Ball Fitting*****************************************//


void combineSobelMagnitudeGradientIPPWrapQ15(unsigned char *original,
											 dtypeipe *magnitude, float *angle,dtypeipe *wip1, dtypeipe *wip2,
											 unsigned int width,
											 unsigned int height)
{ 

int x,y;
float temp1,temp2;
#ifdef USE_IPP
		IppStatus status;
		IppiSize roiSz,dstRoiSize;//,roiSize;
		IppiMaskSize maskSize = ippMskSize3x3;
		int  bufferSize;
		Ipp8u* pBuffer;

		roiSz.width = width;
		roiSz.height = height;
		dstRoiSize.width = width;
		dstRoiSize.height = height;
		status=ippiGradientVectorGetBufferSize (roiSz, maskSize,
			ipp32f, 1, &bufferSize);			
		pBuffer = ippsMalloc_8u(bufferSize);
		status = ippiGradientVectorSobel_8u16s_C1R( original, width*sizeof(char),
			wip2, width*sizeof(dtypeipe), wip1, width*sizeof(dtypeipe), magnitude, width*sizeof(dtypeipe), angle, width*sizeof(float),
			dstRoiSize, maskSize, ippNormL2, ippBorderConst, 0.0f, pBuffer);
		
		status =  ippiMulC_32f_C1IR( -57.2958f, angle, width*sizeof(float), roiSz);
		ippsFree(pBuffer);

// neon intrinsics
#else
#ifdef USE_NEON1
		
		//int16x4_t w1x={0x0080,0x0080,0x0080,0x0080}; //1
		//int16x4_t w2x={0x0000,0x0000,0x0000,0x0000};  //0
		//int16x4_t w3x={0xFF80,0xFF80,0xFF80,0xFF80}; // -1	
		//int16x4_t w4x={0x0101,0x0101,0x0101,0x0101}; //2
		//int16x4_t w5x={0xFEFF,0xFEFF,0xFEFF,0xFEFF}; //-2
		//int16x4_t w6x={0x0080,0x0080,0x0080,0x0080}; //1
		//int16x4_t w7x={0x0000,0x0000,0x0000,0x0000};  //0
		//int16x4_t w8x={0xFF80,0xFF80,0xFF80,0xFF80}; // -1

		//int16x4_t w1x={1,1,1,1}; //1
		//int16x4_t w2x={0,0,0,0};  //0
		//int16x4_t w3x={-1,-1,-1,-1}; // -1	
		//int16x4_t w4x={2,2,2,2}; //2
		//int16x4_t w5x={-2,-2,-2,-2}; //-2
		//int16x4_t w6x={1,1,1,1}; //1
		//int16x4_t w7x={0,0,0,0};  //0
		//int16x4_t w8x={-1,-1,-1,-1}; // -1

		int16x4_t w1x=vdup_n_s16(1); //1
		int16x4_t w2x=vdup_n_s16(0);  //0
		int16x4_t w3x=vdup_n_s16(-1); // -1	
		int16x4_t w4x=vdup_n_s16(2); //2
		int16x4_t w5x=vdup_n_s16(-2); //-2
		int16x4_t w6x=vdup_n_s16(1); //1
		int16x4_t w7x=vdup_n_s16(0);  //0
		int16x4_t w8x=vdup_n_s16(-1); // -1
		
		float ang_rad = 57.2958f;
		//int16x4_t w1y=1,w2y=2,w3y=1,w4y=0,w5y=0,w6y=-1,w7y=-2,w8y=-1;
		//int16x4_t w1y={0x0080,0x0080,0x0080,0x0080}; //1
		//int16x4_t w2y={0x0101,0x0101,0x0101,0x0101}; //2
		//int16x4_t w3y={0x0080,0x0080,0x0080,0x0080}; //1
		//int16x4_t w4y={0x0000,0x0000,0x0000,0x0000};  //0
		//int16x4_t w5y={0x0000,0x0000,0x0000,0x0000};  //0
		//int16x4_t w6y={0xFF80,0xFF80,0xFF80,0xFF80}; // -1
		//int16x4_t w7y={0xFEFF,0xFEFF,0xFEFF,0xFEFF}; //-2
		//int16x4_t w8y={0xFF80,0xFF80,0xFF80,0xFF80}; // -1

		int16x4_t w1y={1,1,1,1}; //1
		int16x4_t w2y={2,2,2,2};  //2
		int16x4_t w3y={1,1,1,1}; // 1	
		int16x4_t w4y={0,0,0,0}; //0
		int16x4_t w5y={0,0,0,0}; //0
		int16x4_t w6y={-1,-1,-1,-1}; //-1
		int16x4_t w7y={-2,-2,-2,-2};  //-2
		int16x4_t w8y={-1,-1,-1,-1}; // -1

		for (int y=1;y<height-1;y++)

		{
		//for (int x=1;x<=width-1-((width-2)%8);x+=8)
		for (int x=1;x<width-1;x+=4)
		{
		//if (x>width-6)
		//{
		//do usual computation

		//}
		//printf("x is %d \n",x);
		if (x < (width-4))  //101
		{
		int32x4_t Ix={0,0,0,0};
		int32x4_t Iy={0,0,0,0};



		int16x4_t v1=vld1_s16( (const dtypeipe *)(original+(y-1)*width+x-1));
		int16x4_t v2=vld1_s16(  (const dtypeipe *)(original+(y-1)*width+x));
		int16x4_t v3=vld1_s16(  (const dtypeipe *)(original+(y-1)*width+x+1));
		int16x4_t v4=vld1_s16(  (const dtypeipe *)(original+(y)*width+x-1));
		int16x4_t v5=vld1_s16(  (const dtypeipe *)(original+(y)*width+x+1));
		int16x4_t v6=vld1_s16(  (const dtypeipe *)(original+(y+1)*width+x-1));
		int16x4_t v7=vld1_s16(  (const dtypeipe *)(original+(y+1)*width+x));
		int16x4_t v8=vld1_s16( (const dtypeipe *)(original+(y+1)*width+x+1));
		
		//Ix=vmlal_s16(Ix,v1,w1x);
		Ix=vqdmlal_s16(Ix,v1,w1x);
		Ix=vqdmlal_s16(Ix,v2,w2x);
		Ix=vqdmlal_s16(Ix,v3,w3x);
		Ix=vqdmlal_s16(Ix,v4,w4x);
		Ix=vqdmlal_s16(Ix,v5,w5x);
		Ix=vqdmlal_s16(Ix,v6,w6x);
		Ix=vqdmlal_s16(Ix,v7,w7x);
		Ix=vqdmlal_s16(Ix,v8,w8x);

		Iy=vqdmlal_s16(Iy,v1,w1y);
		Iy=vqdmlal_s16(Iy,v2,w2y);
		Iy=vqdmlal_s16(Iy,v3,w3y);
		Iy=vqdmlal_s16(Iy,v4,w4y);
		Iy=vqdmlal_s16(Iy,v5,w5y);
		Iy=vqdmlal_s16(Iy,v6,w6y);
		Iy=vqdmlal_s16(Iy,v7,w7y);
		Iy=vqdmlal_s16(Iy,v8,w8y);
		

		//vst1q_f32((wip2+y*width+x),Iy);
		//vst1q_f32((wip1+y*width+x),Ix);
		/*
		for (j=0;j<4;j++)
		{
		Ix[j]=v1[j]*w1x[j]+v2[j]*w2x[j]+v3[j]*w3x[j]+v4[j]*w4x[j]+v5[j]*w5x[j]+v6[j]*w6x[j]+v7[j]*w7x[j]+v8[j]*w8x[j];
		Iy[j]=v1[j]*w1y[j]+v2[j]*w2y[j]+v3[j]*w3y[j]+v4[j]*w4y[j]+v5[j]*w5y[j]+v6[j]*w6y[j]+v7[j]*w7y[j]+v8[j]*w8y[j];
		
		}
		*/

		*(angle + x + y*width) =(ang_rad* atan2f(Iy[0], Ix[0]));
		*(angle + x + y*width+1) =( ang_rad* atan2f(Iy[1], Ix[1]));
		*(angle + x + y*width+2) =( ang_rad* atan2f(Iy[2], Ix[2]));
		*(angle + x + y*width+3) = (ang_rad* atan2f(Iy[3], Ix[3]));
		//*(angle + x + y*width+4) = (ang_rad* atan2(Iy[4], Ix[4]));
		//*(angle + x + y*width+5) = (ang_rad*atan2(Iy[5], Ix[5]));
		//*(angle + x + y*width+6) = (ang_rad*atan2(Iy[6], Ix[6]));
		//*(angle + x + y*width+7) = (ang_rad* atan2(Iy[7], Ix[7]));

		//Iy=vmulq_s16(Iy,Iy);
		//Iy=vmlaq_s16(Iy,Ix,Ix);
		//vst1q_s16((magnitude+y*width+x),Iy);
			
		//*(magnitude + x + y*width) =(dtypeipe)sqrtf(multfix16(Iy[0],Iy[0]) + multfix16(Ix[0],Ix[0]));
		//*(magnitude + x + y*width+1) =(dtypeipe)sqrtf(multfix16(Iy[1],Iy[1]) + multfix16(Ix[1],Ix[1]));
		//*(magnitude + x + y*width+2) = (dtypeipe)sqrtf(multfix16(Iy[2],Iy[2]) + multfix16(Ix[2],Ix[2]));
		//*(magnitude + x + y*width+3) = (dtypeipe)sqrtf(multfix16(Iy[3],Iy[3]) + multfix16(Ix[3],Ix[3]));
		*(magnitude + x-2 + y*width) =(dtypeipe)(sqrtf(Iy[0]*Iy[0] + Ix[0]*Ix[0]));
		*(magnitude + x-2 + y*width+1) =(dtypeipe)(sqrtf(Iy[1]*Iy[1] + Ix[1]*Ix[1]));
		*(magnitude + x-2 + y*width+2) = (dtypeipe)(sqrtf(Iy[2]*Iy[2] + Ix[2]*Ix[2]));
		*(magnitude + x-2 + y*width+3) = (dtypeipe)(sqrtf(Iy[3]*Iy[3] + Ix[3]*Ix[3]));
		//*(magnitude + x + y*width+4) = (dtypeipe)sqrtf(Iy[4]*Iy[4] + Ix[4]*Ix[4]);
		//*(magnitude + x + y*width+5) = (dtypeipe)sqrtf(Iy[5]*Iy[5] + Ix[5]*Ix[5]);
		//*(magnitude + x + y*width+6) = (dtypeipe)sqrtf(Iy[6]*Iy[6] + Ix[6]*Ix[6]);
		//*(magnitude + x + y*width+7) = (dtypeipe)sqrtf(Iy[7]*Iy[7] + Ix[7]*Ix[7]);

		//*(magnitude + x + y*width) =sqrt(Iy[0]);
		//*(magnitude + x + y*width+1) =sqrt(Iy[1]);;
		//*(magnitude + x + y*width+2) = sqrt(Iy[2]);
		//*(magnitude + x + y*width+3) = sqrt(Iy[3]);
		//*(magnitude + x + y*width+4) = sqrt(Iy[4]);
		//*(magnitude + x + y*width+5) = sqrt(Iy[5]);
		//*(magnitude + x + y*width+6) = sqrt(Iy[6]);
		//*(magnitude + x + y*width+7) = sqrt(Iy[7]);

		//*(angle + x + y*width) = 57.2958f* atan2f((*(wip2 + x + y*width)), (*(wip1 + x + y*width)));
		//*(angle + x + y*width+1) = 57.2958f* atan2f((*(wip2 + x + y*width+1)), (*(wip1 + x + y*width+1)));
		//*(angle + x + y*width+2) = 57.2958f* atan2f((*(wip2 + x + y*width+2)), (*(wip1 + x + y*width+2)));
		//*(angle + x + y*width+3) = 57.2958f* atan2f((*(wip2 + x + y*width+3)), (*(wip1 + x + y*width+3)));
		}

		else
		{
		// sliding the x index by 2 to accomodate all the indices
		
		int32x4_t Ix={0,0,0,0};
		int32x4_t Iy={0,0,0,0};



		int16x4_t v1=vld1_s16( (const dtypeipe *)(original+(y-1)*width+x-1-2));
		int16x4_t v2=vld1_s16(  (const dtypeipe *)(original+(y-1)*width+x-2));
		int16x4_t v3=vld1_s16(  (const dtypeipe *)(original+(y-1)*width+x+1-2));
		int16x4_t v4=vld1_s16(  (const dtypeipe *)(original+(y)*width+x-1-2));
		int16x4_t v5=vld1_s16(  (const dtypeipe *)(original+(y)*width+x+1-2));
		int16x4_t v6=vld1_s16(  (const dtypeipe *)(original+(y+1)*width+x-1-2));
		int16x4_t v7=vld1_s16(  (const dtypeipe *)(original+(y+1)*width+x-2));
		int16x4_t v8=vld1_s16( (const dtypeipe *)(original+(y+1)*width+x+1-2));
		
		//Ix=vmlal_s16(Ix,v1,w1x);
		Ix=vqdmlal_s16(Ix,v1,w1x);
		Ix=vqdmlal_s16(Ix,v2,w2x);
		Ix=vqdmlal_s16(Ix,v3,w3x);
		Ix=vqdmlal_s16(Ix,v4,w4x);
		Ix=vqdmlal_s16(Ix,v5,w5x);
		Ix=vqdmlal_s16(Ix,v6,w6x);
		Ix=vqdmlal_s16(Ix,v7,w7x);
		Ix=vqdmlal_s16(Ix,v8,w8x);
		
		
		Iy=vqdmlal_s16(Iy,v1,w1y);
		Iy=vqdmlal_s16(Iy,v2,w2y);
		Iy=vqdmlal_s16(Iy,v3,w3y);
		Iy=vqdmlal_s16(Iy,v4,w4y);
		Iy=vqdmlal_s16(Iy,v5,w5y);
		Iy=vqdmlal_s16(Iy,v6,w6y);
		Iy=vqdmlal_s16(Iy,v7,w7y);
		Iy=vqdmlal_s16(Iy,v8,w8y);
		/*
		for (j=0;j<4;j++)
		{
		Ix[j]=v1[j]*w1x[j]+v2[j]*w2x[j]+v3[j]*w3x[j]+v4[j]*w4x[j]+v5[j]*w5x[j]+v6[j]*w6x[j]+v7[j]*w7x[j]+v8[j]*w8x[j];
		
		Iy[j]=v1[j]*w1y[j]+v2[j]*w2y[j]+v3[j]*w3y[j]+v4[j]*w4y[j]+v5[j]*w5y[j]+v6[j]*w6y[j]+v7[j]*w7y[j]+v8[j]*w8y[j];
		
		}
		*/
		//vst1q_f32((wip2+y*width+x),Iy);
		//vst1q_f32((wip1+y*width+x),Ix);



		*(angle + x-2 + y*width) =(ang_rad* atan2f(Iy[0], Ix[0]));
		*(angle + x-2 + y*width+1) =( ang_rad* atan2f(Iy[1], Ix[1]));
		*(angle + x-2 + y*width+2) =( ang_rad* atan2f(Iy[2], Ix[2]));
		*(angle + x-2 + y*width+3) = (ang_rad* atan2f(Iy[3], Ix[3]));
		//*(angle + x-2 + y*width+4) = (ang_rad* atan2(Iy[4], Ix[4]));
		//*(angle + x-2 + y*width+5) = (ang_rad* atan2(Iy[5], Ix[5]));
		//*(angle + x-2 + y*width+6) = (ang_rad* atan2(Iy[6], Ix[6]));
		//*(angle + x-2 + y*width+7) = (ang_rad* atan2(Iy[7], Ix[7]));

		//Iy=vmulq_s16(Iy,Iy);
		//Iy=vmlaq_s16(Iy,Ix,Ix);
		//vst1q_s16((magnitude+y*width+x-2),Iy);
		
		//*(magnitude + x-2 + y*width) =(dtypeipe)sqrtf(multfix16(Iy[0],Iy[0]) + multfix16(Ix[0],Ix[0]));
		//*(magnitude + x-2 + y*width+1) =(dtypeipe)sqrtf(multfix16(Iy[1],Iy[1]) + multfix16(Ix[1],Ix[1]));
		//*(magnitude + x-2 + y*width+2) = (dtypeipe)sqrtf(multfix16(Iy[2],Iy[2]) + multfix16(Ix[2],Ix[2]));
		//*(magnitude + x-2 + y*width+3) = (dtypeipe)sqrtf(multfix16(Iy[3],Iy[3]) + multfix16(Ix[3],Ix[3]));
		*(magnitude + x-2 + y*width) =(dtypeipe)(sqrtf(Iy[0]*Iy[0] + Ix[0]*Ix[0]));
		*(magnitude + x-2 + y*width+1) =(dtypeipe)(sqrtf(Iy[1]*Iy[1] + Ix[1]*Ix[1]));
		*(magnitude + x-2 + y*width+2) = (dtypeipe)(sqrtf(Iy[2]*Iy[2] + Ix[2]*Ix[2]));
		*(magnitude + x-2 + y*width+3) = (dtypeipe)(sqrtf(Iy[3]*Iy[3] + Ix[3]*Ix[3]));
		//*(magnitude + x-2 + y*width+4) = (dtypeipe)sqrtf(Iy[4]*Iy[4] + Ix[4]*Ix[4]);
		//*(magnitude + x-2 + y*width+5) = (dtypeipe)sqrtf(Iy[5]*Iy[5] + Ix[5]*Ix[5]);
		//*(magnitude + x-2 + y*width+6) = (dtypeipe)sqrtf(Iy[6]*Iy[6] + Ix[6]*Ix[6]);
		//*(magnitude + x-2 + y*width+7) = (dtypeipe)sqrtf(Iy[7]*Iy[7] + Ix[7]*Ix[7]);


		//*(magnitude + x-2 + y*width) =sqrt(Iy[0]);
		//*(magnitude + x-2 + y*width+1) =sqrt(Iy[1]);;
		//*(magnitude + x-2 + y*width+2) = sqrt(Iy[2]);
		//*(magnitude + x-2 + y*width+3) = sqrt(Iy[3]);
		//*(magnitude + x-2 + y*width+4) = sqrt(Iy[4]);
		//*(magnitude + x-2 + y*width+5) = sqrt(Iy[5]);
		//*(magnitude + x-2 + y*width+6) = sqrt(Iy[6]);
		//*(magnitude + x-2 + y*width+7) = sqrt(Iy[7]);

		}



		}
		}


// cpu sequential implementation
#else
		
				for (y = 1; y < height-1; y++)
				for ( x = 1; x < width-1;x++)
				{
				// x gradient
				temp1 = (*(original + (y - 1)*width+ x - 1) +
					2 * (*(original + y*width + x - 1)) +
					*(original + (y+1)*width + x - 1) -
					*(original + (y-1)*width + x + 1) -
					2 *(*(original + (y)*width + x + 1)) -
					*(original + (y+1)*width + x + 1));
				// y grad
				temp2 = (*(original + (y - 1)*width+ x - 1) +
					2 * (*(original + (y-1)*width + x )) +
					*(original + (y - 1)*width + x + 1) -
					*(original + (y + 1)*width + x - 1) -

					2 * (*(original + (y+1)*width + x )) -
					*(original + (y + 1)*width + x + 1));
			
				
				*(magnitude + x + y*width) = (dtypeipe)(sqrtf(temp1*temp1 + temp2*temp2));
				*(angle + x + y*width) = (57.2958f* atan2f(temp2, temp1));  //57.2958

				//fprintf(fp,"val at x,y:%d,%d is %f\n",x,y,*(wip1+ x+y*width));
				}

#endif
#endif
}





// CPU Optimized
void imageGradientBallFinding2Q15(float* image,dtypeipe *edgeStrength,dtypeipe *radiusTemplate, 
							   dtypeipe *heatMap,unsigned short *thsCol,unsigned short *thsRow,
							   unsigned short thsLength,
							  short *gradMap,
							  unsigned short *gradX, unsigned short *gradY,float currentRadius,
							  unsigned short mapLength, unsigned int height, unsigned int width,
							  unsigned int templateHeight, unsigned int templateWidth,
							  float *centerRow, float *centerCol, float *score,float thresh)
{
	float sum=-1;
	float currentMin = 90*mapLength;
	int counter=0;
	float currentScore = 0.0f,diff1,diff2,diff3,diff;
	unsigned int midRow = ((templateHeight/2.0f)+0.5f);
	unsigned int midCol = ((templateWidth/2.0f)+0.5f);
	unsigned int currentIndex;
	int thsIndex,mapIndex,colIndex,rowIndex,indX,indY;
	float left,right,top,bottom;
	int leftX,rightX,topY,bottomY;
	unsigned int W,H;
	dtypeipe edge_threshold=thresh; //0.01f
	// -2 = TH_1_BF - TH_5_BF  LITH
	// -0.25 = TH_2_BF - TH_5_BF  L2TH
	// 0 = TH_3_BF - TH_5_BF L3TH
	// 0.5 = TH_4_BF - TH_5_BF L4TH
	// 0.75 = TH_5_BF
	float Global_bin[]={-2,-2,-0.25,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,-0.25,-2,-2,-0.25,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,-0.25,-2};
	//float Global_bin[]={L1TH,L1TH,L2TH,L3TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L3TH,L2TH,L1TH,L1TH,L2TH,L3TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L4TH,L3TH,L2TH,L1TH};

	//float Global_bin[]={-1.25,-1.25,-0.25,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,-0.25,-1.25,-1.25,-0.25,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0,-0.25,-1.25};
	float edgeScore = 0.0f;
	float aggregate = 0.0f;
	#ifdef USE_IPP
	//edge_threshold=0.1;
	#endif

	// Bin based scores for diff1, 36 bins with binsize of 10 each. 
	//float Global_bin[]={-1.25,0.5,0.75,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,0.75,0.5,-1.25,-1.25,0.5,0.75,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,1.25,0.75,0.5,-1.25};
	*centerRow = 0;
	*centerCol = 0;
	W = width - templateWidth;
	H = height - templateHeight;

	//diff3 = currentMin;
	for(thsIndex=0;thsIndex<thsLength;thsIndex++)
	{	
		//currentScore = 0.0f;
		currentScore=0.75*mapLength; // pre loading the score so that we only have to adjust score for strong edge pixels
		colIndex = *(thsCol+thsIndex)-midCol; //X-coordinate
		rowIndex = *(thsRow+thsIndex)-midRow; //Y-coordinate
		//indX = *(thsCol+thsIndex); // col
		//indY = *(thsRow+thsIndex); // row
		//topY = indY - (int)currentRadius + 5;
		//bottomY = indY + (int)currentRadius - 5;
		//rightX = indX + (int)currentRadius - 5;
		//leftX = indX - (int)currentRadius + 5;
		{
			if(colIndex>-1&&rowIndex>-1&&colIndex<W&&rowIndex<H)
    		{  
				for(mapIndex=0;mapIndex<mapLength;mapIndex++)
				{
					currentIndex = colIndex+1+*(gradX+mapIndex)+
						(rowIndex+1+*(gradY+mapIndex))*width;

			
					if((*(edgeStrength+currentIndex))>edge_threshold)
					{    
						counter+=1;
						diff1 = fabsf(*(gradMap+mapIndex)-*(image+currentIndex));

						currentScore=currentScore+Global_bin[(int)ceil(diff1*0.1)];

					}
					//edgeScore += *(edgeStrength+currentIndex);
				}
				//aggregate = 8192.0f*(currentScore - edgeScore/100)/mapLength;
				aggregate = 8192.0f*(currentScore)/mapLength;
				if(*(heatMap+*(thsCol+thsIndex)+(*(thsRow+thsIndex))*width)>(dtypeipe)(aggregate))
				{
					*(heatMap+*(thsCol+thsIndex)+(*(thsRow+thsIndex))*width) = 
						(dtypeipe)(aggregate);
					//if (*(thsCol+thsIndex)==884 && *(thsRow+thsIndex)==282)
					//printf("binary image value is %f and current radius is %f and value is %f\n", *(binaryImage+(*(thsCol+thsIndex))+(*(thsRow+thsIndex))*width),currentRadius,currentScore/mapLength);
					*(radiusTemplate+*(thsCol+thsIndex)+(*(thsRow+thsIndex))*width) = (dtypeipe)currentRadius;
				}
				if(currentScore<currentMin)
				{
					currentMin = currentScore;
					*centerCol = *(thsCol+thsIndex);//+midCol;
					*centerRow = *(thsRow+thsIndex);//+midRow;
				}	
			}
		}
	}
	*score = currentMin/mapLength;
}



void imageFind2DLocalMinOptimizedQ15(dtypeipe *image, unsigned int width, unsigned int height, 
								  unsigned int windowSize,int arrayLength,
								  unsigned short *rowIndex, unsigned short *colIndex,
								  int indexLength,float *colArray, 
								  float *rowArray)
{
	int i,j,k,previousCol=windowSize,previousRow=windowSize,currentCol,currentRow;
	dtypeipe peakValue[25];
	dtypeipe tempVal;
	
	for(i=0;i<arrayLength;i++)
	{
		peakValue[i] = 0x7fff;
		*(colArray+i) = 0;
		*(rowArray+i) = 0;
		for(j=0;j<indexLength;j++)
		{
			currentCol = *(colIndex+j);
			currentRow = *(rowIndex+j);
#ifdef MEM_INDEX
			tempVal = *(image+_IMGIndex[currentRow][currentCol]);
#else
			tempVal = *(image+currentRow*width+currentCol);
#endif
				if(tempVal < peakValue[i])
				{
					//peakValue[i] = *(image+currentRow*width+currentCol);
					peakValue[i] = tempVal;//*(image+_IMGIndex[currentRow][currentCol]);
					*(colArray+i) = currentCol;
					*(rowArray+i) = currentRow;
					previousCol = currentCol;
					previousRow = currentRow;
				}
		}
		//to wipe out previous global min
		if(*(colArray+i)>0)
		{
			for(j=previousCol-windowSize;j<previousCol+windowSize+1;j++)
			{
				for(k=previousRow-windowSize;k<previousRow+windowSize+1;k++)
				{
#ifdef MEM_INDEX
					*(image+_IMGIndex[k][j]) = 10.0f;
#else
					*(image+k*width+j) = 0x7fff;
#endif
				}
			}
		}
	}
}



//**************************************************** Edge Detecor - Ball Fitting*****************************************//



//**************************************************** Image Setters *****************************************//


static void  imgSetQ15(RAPSODO_IMAGE_TYPE *img, dtypeipe value)
{
	unsigned int row;
	for (row=0;row < img->height * img->width ; row++)
	{
		*(img->image+row)=value;
	}
}


//**************************************************** Image Setters *****************************************//

#endif




