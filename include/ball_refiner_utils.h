#ifndef __BALL_REFINER_UTILS_H__
#define __BALL_REFINER_UTILS_H__

#define dtypeipe short
#define BASEBALL_WIP_NUMBER 6

#define GRADIENT_FIT_WIDTH 20        // 10

#define BASEBALL_MAX_IMAGES_PITCHING 1
#define BASEBALL_MAX_IMAGES BASEBALL_MAX_IMAGES_PITCHING
#define IMAGE_MAX_PIXEL_VALUE 255
#define FP_CONST 32768
#define TEMPLATE_END_SOFTBALL 70 //75
#define TEMPLATE_START 10            // 14
#define TEMPLATE_SIZE TEMPLATE_END_SOFTBALL - TEMPLATE_START

#define ZERO_MEMORY(x, y) memset(x, 0, y)
#define MAX_IMAGE_HEIGHT 650

#define numOfImgThreads 1
#define TEMPLATE_LENGTH_LIMIT 1000
#define MATH_PI 3.14159265359f

//**************************************************** Data Structs *****************************************//
typedef enum {
  RIPE_SUCCESS,
  RIPE_ERROR,
  RIPE_ERROR_NO_BALL,
  RIPE_ERROR_NO_SPIN,
  RIPE_ERROR_NO_SPEED,
  RIPE_ERROR_LAST_TYPE
} RAPSODO_ERROR_TYPE;



typedef struct {
  unsigned short *xPosition;
  unsigned short *yPosition;
  short *gradientValue;
  unsigned short radius;
  unsigned short templateLength;
} RAPSODO_GRADIENT_TEMPLATE_TYPE;


typedef enum {
  RIPE_BALL_FOUND,
  RIPE_BALL_CROPPED,
  RIPE_BALL_NOT_FOUND,
  RIPE_BALL_FOUND_LAST_TYPE
} RAPSODO_BALL_FOUND_TYPE;




typedef struct {
  unsigned int width;
  unsigned int height;
  float *image;

} RAPSODO_IMAGE_TYPE_FLOAT;

typedef struct {
  unsigned int width;
  unsigned int height;
  dtypeipe *image;

} RAPSODO_IMAGE_TYPE;

typedef struct {
  unsigned int height;
  unsigned int width;
  unsigned char *image;

} RAPSODO_INT_IMAGE_TYPE;


typedef struct {
  float ballConfidence;
  float ballRadius;
  float centerX;
  float centerY;
} RAPSODO_BALL_PARAM;

typedef struct {
  RAPSODO_IMAGE_TYPE currentImage;
  RAPSODO_IMAGE_TYPE absDiffImage;
  RAPSODO_IMAGE_TYPE croppedImage;
  RAPSODO_IMAGE_TYPE_FLOAT spinImage;
  RAPSODO_INT_IMAGE_TYPE claheImage;

// #ifdef MEMORY_OPTIMIZATION
//   RAPSODO_IMAGE_TYPE *WIP;
// #else
  RAPSODO_IMAGE_TYPE WIP[BASEBALL_WIP_NUMBER];  // Taken out for MEMORY OPTIMIZATION
// #endif

  RAPSODO_BALL_PARAM ball;
  RAPSODO_BALL_FOUND_TYPE ballFound;
  int index;
  float pixelPositionX[GRADIENT_FIT_WIDTH];
  float pixelPositionY[GRADIENT_FIT_WIDTH];
  float imagingRadius[GRADIENT_FIT_WIDTH];
} RAPSODO_PROCESS_BALL_TYPE;

typedef struct {
  RAPSODO_PROCESS_BALL_TYPE ballForProcessing[BASEBALL_MAX_IMAGES];
  RAPSODO_IMAGE_TYPE WIP[BASEBALL_WIP_NUMBER * BASEBALL_MAX_IMAGES];
  RAPSODO_IMAGE_TYPE_FLOAT WIPFLOAT[BASEBALL_MAX_IMAGES * BASEBALL_WIP_NUMBER];
  unsigned short ballFoundCount;
  int isOutdoor;
  int level1BallCount;
  RAPSODO_GRADIENT_TEMPLATE_TYPE gradientTemplateArray[TEMPLATE_SIZE];

} RAPSODO_ENGINE_TYPE;

//**************************************************** Data Structs *****************************************//

#endif
