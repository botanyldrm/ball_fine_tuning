// copyright rapsodo 2012
// batu
// aug 2012

#ifndef __CLAHE_H
#define __CLAHE_H
//#include "obfuscate.h"
// #include "ipeBaseballRef.h"
#ifdef __cplusplus
extern "C" {  
#endif 

#ifdef TANGENT_POINTS
#define CLAHE_X_RES					200
#define CLAHE_Y_RES					40
#define CLAHE_MIN_PIX_VAL			0
#define CLAHE_MAX_PIX_VAL			255
#define CLAHE_X_REG_CTXT			20
#define CLAHE_Y_REG_CTXT			10
#define CLAHE_BIN_COUNT				256
#define CLAHE_CLIP_LIMIT			255*0.01f //255*0.01f
#else
#define CLAHE_X_RES					80
#define CLAHE_Y_RES					80
#define CLAHE_MIN_PIX_VAL			0
#define CLAHE_MAX_PIX_VAL			255
#define CLAHE_X_REG_CTXT			8
#define CLAHE_Y_REG_CTXT			8
#define CLAHE_BIN_COUNT				256
#define CLAHE_CLIP_LIMIT			255*0.025f
#endif

typedef struct
{
	unsigned short widthRes;
	unsigned short heightRes;
	unsigned short minPixValue;
	unsigned short maxPixValue;
	unsigned short widthQuantum;
	unsigned short heightQuantum;
	unsigned short bins;
	float clipLimit;
}claheParamsType;


// type defs. for Graphic Gemms Code - see later
#define BYTE_IMAGE //Batu
#ifdef BYTE_IMAGE
typedef unsigned char kz_pixel_t;        /* for 8 bit-per-pixel images */
#define uiNR_OF_GREY (256)
#else
typedef unsigned short kz_pixel_t;       /* for 12 bit-per-pixel images (default) */
# define uiNR_OF_GREY (1024)   /* Rapsodo modified for 10 bit images */
#endif

	
	int CLAHE(kz_pixel_t* pImage, unsigned int uiXRes, 
		  unsigned int uiYRes, kz_pixel_t Min,
          kz_pixel_t Max, unsigned int uiNrX, unsigned int uiNrY,
          unsigned int uiNrBins, float fCliplimit);





#ifdef __cplusplus
}
#endif

#endif
